//
//  Global.swift
//  yellowCar
//
//  Created by Mohamed anwar on 6/29/16.
//  Copyright © 2016 Mohamed anwar. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import ReachabilitySwift





let testN = NSNotification.Name("test")
let DeleteNotivcationName = NSNotification.Name("delete")
let acceptNotivcationName = NSNotification.Name("accept")
let DeleteMsgNotivcationName = NSNotification.Name("deletemsg")
let DeleteFriendNotivcationName = NSNotification.Name("deleteFriend")
let refechNotivcationName = NSNotification.Name("refechNotivcation")





var openForFirstTime = "openForFirstTime"
var CityId = "CityId"
var emailKey = "email"


var baseURl = "http://www.waelmoharram.com"
var apiURL = "/api/v1"
var mainurl = baseURl+apiURL
var tokenUrl = "?api_token="
//request Cities
//no params
var citiesUrl = "/cities"
//main category
//no params
var mainCategoryUrl = "maincategories"
//sub category
//upper_id = main category item id
var subCategoryUrk = "/subcategories?upper_id="
//Returns lists
//subcategory
//origin = lat,lang
//city_id = city id
var ListsUrl = "/category"
//list detail
//list id
var List_DetailUrl = "/listing"
//search

//origin = lat,lang
//name = search text
//main_category = main category id
//sub_category = sub category id
//city_id = city id
//orderby =
// rate : وهو بالاعلى تقييما
//visits : الاعلى مشاهدة
//created_at : الاحدث
var Search_ResultUrl = "/search"
//register for notification
//token = api token
// type = 2
var notifyUrl = "/notification?token="
//rating
//id_type = 2
//stars = stars
//id_social = social email
//listing_id = list id
var setRateUrl = "/rate?id_type="

//order by
//orderby =
// rate : وهو بالاعلى تقييما
//visits : الاعلى مشاهدة
//created_at : الاحدث
//origin = lat,lang
var viewByUrl = "/search?orderby="
//send message
//name = text name
//email = registered mail
//title = registered title
//text = subject
var contactUsUrl = "/message"
//view all notifications
var getNotificationsUrl = "/nm"








class GlobalViewController: UIViewController {
    
    func sendBody1(_ myUrl:String,premeters :[String : String ], type : String)  {
        SwiftLoading().showLoading()
        
        Alamofire.request(myUrl, method: .post, parameters: premeters, encoding: JSONEncoding.default)
            .validate(statusCode: 200..<300)
            
            .validate(contentType: ["application/json"])
            .responseData { response in
                
                var statusCode = response.response?.statusCode
                var data = response.data
                
                let error = response.result.error as? AFError
                
                
                if error == nil && statusCode == 200  {
                    SwiftLoading().hideLoading()
                    
                    OperationQueue.main.addOperation({
                        
                        self.getrespons(data! , type: type)
                        
                    })
                    
                    
                }else{
                    //                    print("here is error",error)
                    SwiftLoading().hideLoading()
                    
                    let alert = UIAlertController(title: "Error", message: "server Problem", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Try Again Later", style: .cancel, handler: nil))
                    
                    self.present(alert, animated: true, completion: nil)
                    
                }
        }

    }
    
//    Alamofire.upload(
//    multipartFormData: { multipartFormData in
//    multipartFormData.append(unicornImageURL, withName: "unicorn")
//    multipartFormData.append(rainbowImageURL, withName: "rainbow")
//    },
//    to: "https://httpbin.org/post",
//    encodingCompletion: {
    
    
    
//    func sendImageBody(_ url : String ,MainImage : Data?, arrayOfImages :[ Data?],prameter : [String:String]) {
    func sendImageBody(_ url : String ,MainImage : Data?,prameter : [String:String]) {

        SwiftLoading().showLoading()
//        var mages : [Data?] = arrayOfImages
        Alamofire.upload( multipartFormData: {
                multipartFormData in
            
            multipartFormData.append(MainImage!, withName: "img", fileName: "1.jpeg", mimeType: "image/jpeg")
            
            
            
            
            
//                for (key) in mages {
//                    multipartFormData.append(key!, withName: "imgs[]", fileName: "1.jpeg", mimeType: "image/jpeg")
//                    //                    print("1")
//                    
//                }
            
            
            
                for (key , value) in prameter {
                    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                
            },
                          to: url
            , encodingCompletion: { encodingResult in
                print(encodingResult)
                switch encodingResult {
                case .success(let upload, _, _):
                    //                    print("SUCCESS")
                    //                    SwiftLoading().hideLoading()
                    
                    
                    upload.responseJSON { response in
//                        print(response)
                        let dataJ = response.data
                        if let JSON = response.result.value{
//                            print(JSON)
                            self.getImageRespons(dataJ! )
                            SwiftLoading().hideLoading()
                            
                            //                            print("JSON: \(JSON)")
                        }
                    }
                case .failure(let error):
                    print(error)
                }
        })
    }
    
    //====================================================
    func sendProfileBody(_ url : String ,MainImage : Data?,prameter : [String:String])  {
        SwiftLoading().showLoading()
        
        Alamofire.upload(multipartFormData: {
                multipartFormData in
            multipartFormData.append(MainImage!, withName: "img", fileName: "1.jpeg", mimeType: "image/jpeg")
            
                
                for (key , value) in prameter {
                     multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                }
                
                
        },to : url, encodingCompletion: { encodingResult in
                switch encodingResult {
                    
                case .success(let upload, _, _):
                                        print("SUCCESS")
                    SwiftLoading().hideLoading()
                    
                    upload.responseJSON { response in
                        print(response)
                        if let JSON = response.result.value{
                            SwiftLoading().hideLoading()
                            
                            self.getImageRespons(JSON as! Data)
                            //                            print("JSON: \(JSON)")
                        }
                    }
                case .failure(let error):
                    print(error)
                }
        })
    }
    
    
    
    
    
    
 
    
    //=================post request==========================
    func sendReqestWithUrl(_ myUrl : String , premeters :[String : String ], type : String){
        
            let reachability = Reachability()!
            
            switch reachability.currentReachabilityStatus{
            case .reachableViaWiFi:
                sendBody1(myUrl,premeters: premeters,type: type)
                print("Connected With wifi")
            case .reachableViaWWAN:
                sendBody1(myUrl,premeters: premeters,type: type)
                print("Connected With Cellular network(3G/4G)")
            case .notReachable:
                print("Not Connected")
                let alert = UIAlertController(title: "Error", message: "No Internet Connection", preferredStyle: .alert)
                
                            alert.addAction(UIAlertAction(title: "Try Again Later", style: .cancel, handler: nil))
                
                            self.present(alert, animated: true, completion: nil)
            }
        
        
////        if Reachability.isConnectedToNetwork() == true {
//            SwiftLoading().showLoading()
//
//            Alamofire.request(.POST, myUrl, parameters: premeters)
//                .response { (req:NSURLRequest?, res:NSHTTPURLResponse?, data:NSData?, error:NSError?) in
//                    if error == nil && res?.statusCode == 200  {
//                        // handle success
//                        SwiftLoading().hideLoading()
//
//                        NSOperationQueue.mainQueue().addOperationWithBlock({
//                            
//                            self.getrespons(data! , type: type)
//                            
//                        })
//                        
//                        
//                    }else{
////                    print("here is error",error)
//                        SwiftLoading().hideLoading()
//
//                        let alert = UIAlertController(title: "Error", message: "No Internet Connection", preferredStyle: .Alert)
//                        
//                        alert.addAction(UIAlertAction(title: "Try Again Later", style: .Cancel, handler: nil))
//                        
//                        self.presentViewController(alert, animated: true, completion: nil)
//                        
//                    }
//            }
//        }else{
//            
////            print("Internet connection FAILED")
//            
//            let alert = UIAlertController(title: "Error", message: "No Internet Connection", preferredStyle: .Alert)
//            
//            alert.addAction(UIAlertAction(title: "Try Again Later", style: .Cancel, handler: nil))
//            
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }
    }
    //================= post image========================
//    func sendImage(_ url : String ,MainImage : Data?, arrayOfImages :[ Data?],prameter : [String:String]) {
    func sendImage(_ url : String ,MainImage : Data?,prameter : [String:String]) {

        do {
            let reachability = Reachability()!
            
            switch reachability.currentReachabilityStatus{
            case .reachableViaWiFi:
//                sendImageBody(url, MainImage: MainImage, arrayOfImages: arrayOfImages, prameter: prameter)
                sendImageBody(url, MainImage: MainImage, prameter: prameter)

                print("Connected With wifi")
            case .reachableViaWWAN:
                sendImageBody(url, MainImage: MainImage, prameter: prameter)
                
                print("Connected With Cellular network(3G/4G)")
            case .notReachable:
                
                print("Not Connected")
                
                let alert = UIAlertController(title: "Error", message: "No Internet Connection", preferredStyle: .alert)
                
                
                alert.addAction(UIAlertAction(title: "Try Again Later", style: .cancel, handler: nil))
                
                
                self.present(alert, animated: true, completion: nil)
                
            }
            
        }
            
        catch let error as NSError{
            
            print(error.debugDescription)
            
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
//  
//         if ReachabilityS.isConnectedToNetwork() == true {
//        SwiftLoading().showLoading()
//        var mages : [NSData?] = arrayOfImages
//        Alamofire.upload(.POST, url
//            , multipartFormData: {
//                formData in
//                formData.appendBodyPart(data: MainImage!, name: "img",fileName: "1.jpeg",mimeType: "image/jpeg")
//                
//                for (key) in mages {
//                    formData.appendBodyPart(data: key!, name: "imgs[]", fileName: "1.jpeg", mimeType: "image/jpeg")
////                    print("1")
//                    
//                }
//                for (key , value) in prameter {
//                    formData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                }
//                
//                
//            }, encodingCompletion: { encodingResult in
//                switch encodingResult {
//                    
//                case .Success(let upload, _, _):
////                    print("SUCCESS")
////                    SwiftLoading().hideLoading()
//                    
//                    upload.responseJSON { response in
//                        if let JSON = response.result.value{
//
//                            self.getImageRespons(JSON)
//                            SwiftLoading().hideLoading()
//
////                            print("JSON: \(JSON)")
//                        }
//                    }
//                case .Failure(let error):
//                    print(error)
//                }
//        })
//         }else{
//            
////            print("Internet connection FAILED")
//            
//            let alert = UIAlertController(title: "Error", message: "No Internet Connection", preferredStyle: .Alert)
//            
//            alert.addAction(UIAlertAction(title: "Try Again Later", style: .Cancel, handler: nil))
//            
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }
    }
    //=====================================================
    func sendProfileImage(_ url : String ,MainImage : Data?,prameter : [String:String]) {
        
        
        do {
            let reachability = Reachability()!
            
            switch reachability.currentReachabilityStatus{
            case .reachableViaWiFi:
                sendProfileBody(url, MainImage: MainImage, prameter: prameter)
                print("Connected With wifi")
            case .reachableViaWWAN:
                sendProfileBody(url, MainImage: MainImage, prameter: prameter)
                print("Connected With Cellular network(3G/4G)")
            case .notReachable:
                print("Not Connected")
                let alert = UIAlertController(title: "Error", message: "No Internet Connection", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Try Again Later", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        catch let error as NSError{
            print(error.debugDescription)
        }
        
//        if Reachability.isConnectedToNetwork() == true {

//        SwiftLoading().showLoading()
//        
//        Alamofire.upload(.POST, url
//            , multipartFormData: {
//                formData in
//                formData.appendBodyPart(data: MainImage!, name: "img",fileName: "1.jpeg",mimeType: "image/jpeg")
//                
//                
//                for (key , value) in prameter {
//                    formData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
//                }
//                
//                
//            }, encodingCompletion: { encodingResult in
//                switch encodingResult {
//                    
//                case .Success(let upload, _, _):
////                    print("SUCCESS")
//                    SwiftLoading().hideLoading()
//                    
//                    upload.responseJSON { response in
//                        if let JSON = response.result.value{
//                            SwiftLoading().hideLoading()
//
//                            self.getImageRespons(JSON)
////                            print("JSON: \(JSON)")
//                        }
//                    }
//                case .Failure(let error):
//                    print(error)
//                }
//        })
//        }else{
//            
////            print("Internet connection FAILED")
//            
//            let alert = UIAlertController(title: "Error", message: "No Internet Connection", preferredStyle: .Alert)
//            
//            alert.addAction(UIAlertAction(title: "Try Again Later", style: .Cancel, handler: nil))
//            
//            self.presentViewController(alert, animated: true, completion: nil)
//            
//        }
    }
    
    
    //===================get Image Respons===================
    func getImageRespons(_ json : Data){
//        print("here is respons Image")
        
        
        
    }
    
    
    
    
    
    
    func getrequestS(urls : String ,  type : String){

        do {
            let reachability = Reachability()!
            
            switch reachability.currentReachabilityStatus{
            case .reachableViaWiFi:
                getRequest(urls, type: type)
                print("Connected With wifi")
            case .reachableViaWWAN:
                getRequest(urls, type: type)
                print("Connected With Cellular network(3G/4G)")
            case .notReachable:
                print("Not Connected")
                let alert = UIAlertController(title: "Error", message: "No Internet Connection", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Try Again Later", style: .cancel, handler: nil))
                
                self.present(alert, animated: true, completion: nil)
            }
        }
        catch let error as NSError{
            print(error.debugDescription)
        }
        
    
    
    }
    
    
    
    
    
    
    
    //=====================get request =======================
    
    func  getRequest(_ myURL : String , type : String ) {

        SwiftLoading().showLoading()
        Alamofire.request(myURL,method:.get)
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
            .responseData { response in
                let statusCode = response.response?.statusCode
                let data = response.data

                 let error = response.result.error as? AFError
                
                
                if error == nil && statusCode == 200  {
                    // handle success
                    OperationQueue.main.addOperation({
                        SwiftLoading().hideLoading()
                        if type == "image"{
                            self.getresponsImage(data!)
                            
                        }else{
                            self.getrespons(data! , type: type)
                        }
                        
                    })
                }
        }

    
    }
    //=======================Get respons Func==================
    
    
    func getrespons(_ ns : Data , type : String) {
//        print("here is get resposns")
        
        
    }
    //==========
    func getresponsImage (_ ns : Data ) {
//        print("here is get resposns")
        
        
    }
    //=======================ValidateEmail======================
    
    
    func validateEmail(_ candidate: String) -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: candidate)
    }
    
    
    
    //======================Alert MSG ==========================
    func alertDismiss(Msg msg :String , title : String , Dissmiss: String){
        let alertController = UIAlertController(title: title, message:
            msg, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title:Dissmiss , style: UIAlertActionStyle.default,handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    //====================Move to screen with Iditfier ==========
    func goHome(_ id :String){
        SwiftLoading().showLoading()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: id)
        vc.modalTransitionStyle = .flipHorizontal
        self.present(vc, animated: true, completion: nil)
        SwiftLoading().hideLoading()
        
    }
    //==================  [ResizeImage]  ====================
    func ResizeImage(_ image: UIImage, targetSize: CGSize) -> Data? {
        let size = image.size
        
        let widthRatio  = targetSize.width / image.size.width
        let heightRatio = targetSize.height / image.size.height
        //
        //
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let reImage = UIImageJPEGRepresentation(newImage!, 0.9)
        
        return reImage
    }
    //==========================================
    func alertLogin(){
        let alertController = UIAlertController(title: "تنبية", message:
            "يجب تسجيل الدخول", preferredStyle: UIAlertControllerStyle.alert)
        let dismissAction = UIAlertAction.init(title: "إلغاء", style: .cancel, handler: nil)
        let settingsAction = UIAlertAction.init(title: "تسجيل الدخول", style: .default) { (action) in
            self.goHome("LoginViewController")
        }
        alertController.addAction(dismissAction)
        alertController.addAction(settingsAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
//    //========================================
//    func strRandomL(_ len : Int) -> NSString {
//        
//        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
//        
//        let randomString : NSMutableString = NSMutableString(capacity: len)
//        
//         for ( _ in  0..< len){
////            fr(){
//            let length = UInt32 (letters.length)
//            let rand = arc4random_uniform(length)
//            randomString.appendFormat("%C", letters.character(at: Int(rand)))
//        }
//        
//        return randomString
//    }

    //========================================
    func addbagdetobarBut(mesaBut : UIBarButtonItem , navBar : UIBarButtonItem, numOfmsg : String,numOfNot : String)  {
        let intNotNum = Int(numOfNot)
        if numOfNot == "0" {
            
            navBar.removeBadge()
        }else if intNotNum! > 99 {
            navBar.addBadge(number: "99+", withOffset: CGPoint(x: 0 , y: 0), andColor: UIColor.red, andFilled: true)
            
        }else{
            navBar.addBadge(number: numOfNot, withOffset: CGPoint(x: 0 , y: 0), andColor: UIColor.red, andFilled: true)
            
        }
        let intMesNum = Int(numOfmsg)
        if numOfmsg == "0" {
            
            mesaBut.removeBadge()
        }else if intMesNum! > 99 {
            
            mesaBut.addBadge(number: "99+", withOffset: CGPoint(x: 0 , y: 0), andColor: UIColor.red, andFilled: true)
            
        }else{
            mesaBut.addBadge(number: numOfmsg, withOffset: CGPoint(x: 0 , y: 0), andColor: UIColor.red, andFilled: true)
            
        }
    }
    
    
    
    
}


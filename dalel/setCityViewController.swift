//
//  setCityViewController.swift
//  Dalil
//
//  Created by Mohamed on 2/5/17.
//  Copyright © 2017 Panorama EL Qasem. All rights reserved.



//


import UIKit
import SwiftyJSON
class setCityViewController: GlobalViewController {
    var arrayOfCitis = [AnyObject]()
    var counter = 0
    
    var imageUrl : String!
    var city_name :String!
    var id : Int!
    
    
    
    @IBOutlet weak var cityNameLable: UILabel!
    @IBAction func clickRightBut(_ sender: UIButton) {
        if (counter < (arrayOfCitis.count - 1)) {
            counter += 1

            self.setData()
        
        }
    }
    
    @IBAction func clickLeftBut(_ sender: UIButton) {
        if counter > 0 {

        counter -= 1
            self.setData()

        
        }
    }
    
    
    @IBOutlet weak var cityImageView: CustomImageView!
    @IBOutlet weak var chooseCity: UIButton!
    
    @IBAction func chooseCityNameBut(_ sender: UIButton) {
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addBorder(label: cityNameLable, borderWidth: 1, borderColor: .clear, cornerRadius: 5)
        addBorder(button: chooseCity, borderWidth: 1, borderColor: .clear, cornerRadius: 5)
        getrequestS(urls: "http://www.waelmoharram.com/dalel/api/v1/cities", type: "")
        UserDefaults.standard.set(true, forKey: openForFirstTime)
        
        
        
    }
    override func getrespons(_ ns: Data, type: String) {
        let jasn = JSON(data : ns)
        print(jasn)
        arrayOfCitis = jasn.arrayObject as! [AnyObject]
  
        setData()
        
        
        
        
        print(arrayOfCitis.count)
        print(arrayOfCitis[1]["city_image"] as! String)

        
    }
    func setData(){
        if arrayOfCitis.count > 0 {
            imageUrl = arrayOfCitis[counter]["city_image"] as! String
            city_name = arrayOfCitis[counter]["city_name"]  as! String
            id = arrayOfCitis[counter]["id"]  as!Int

        setupImage(imageUrl, imageView: cityImageView)
            cityNameLable.text = city_name
            UserDefaults.standard.set(id, forKey: CityId )
        
        }
        
        
        
        
        
    }
    
    
}
extension UIViewController{
    func addBorder( label : UILabel, borderWidth : Int , borderColor : UIColor , cornerRadius : Int  ) {
        label.layer.borderWidth = CGFloat(borderWidth)
        label.layer.borderColor =  borderColor.cgColor
        label.layer.cornerRadius = CGFloat(cornerRadius)
        label.layer.masksToBounds = true
        
    }
    func addBorder( button : UIButton, borderWidth : Int , borderColor : UIColor , cornerRadius : Int  ) {
        button.layer.borderWidth = CGFloat(borderWidth)
        button.layer.borderColor =  borderColor.cgColor
        button.layer.cornerRadius = CGFloat(cornerRadius)
        button.layer.masksToBounds = true
        
    }

}
